---
layout: documentation
title: Using the community wiki
subtitle: All centralized collaborative documentation in one place.
category: Introduction
order: 1
---

## Any questions? The wiki is here to help!
This documentation is the central place for all your questions. Also be sure to download the OIF-WELCOME-KIT.pdf that will help you setup the system in the first weeks.

You can also access the chat on our discord server here.

## Contribute to enrich the experience
You can always suggest your suggestions to improve our explanations. Use the button: 'suggest edits' to contribute.

## License to use
All content on this Wiki is distributed with a license:
**Attribution - Sharing in the same Terms 4.0 International (CC BY-SA 4.0)**

**You are allowed to:**

**Share** - copy, distribute and communicate the material by any means and in any format  
**Adapt** - remix, transform and create from the material
for any use, including commercial.

This license is acceptable for free cultural works.
The Offeror may not withdraw the authorizations granted by the license as long as you apply the terms of this license.

**According to the following conditions:**

**Attribution** - You must credit the Work, include a link to the license and indicate if any changes have been made to the Work. You must provide this information by all reasonable means, without however suggesting that the Offeror supports you or supports the way you used the Work.

**Sharing under the same conditions** - In the event that you perform a remix, transform, or create from the material composing the original work, you must broadcast the modified work under the same conditions, ie with the same license with which the original Work was broadcast.

**No additional restrictions** - You are not allowed to apply legal conditions or technical measures that legally restrict others to use the Work under the conditions described by the license.

More information [here](https://creativecommons.org/licenses/by-sa/4.0/deed.en)

![img](https://files.readme.io/9b0aa3a-CC-BY-SA_icon.svg.png)
