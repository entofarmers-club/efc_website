---
layout: documentation
title: Model Tinker
subtitle: The basic model for tinkerers
category: Solutions
order: 10
---

{:.is-size-5}
This is the model for tinkerers. A DIY version to experiment with smart insect farming.

## Current version
The current version of OIF Model Tinker is **v1**. This is the very first version, based on an ESP8266 and an DHT11 humidity and temperature sensor. Data is pushed to the backend using the http api.

**Version history**

  * v1: This is the very first version, based on an ESP8266 and an DHT humidity and temperature sensor.

## Welcome
Welcome pioneering insect farmer! Thank you for choosing the OIF Tinker kit. You will be joining a growing group of insect farmers all over the world. Aiming for a sustainable and transparent food supply. Model Tinker makes it possible to start experimenting with insect farming. It is a great way of experiencing what it takes to grow insects, take care of them and learn about biology, food and technology!

Model Tinker is based on open-source and DIY developments in mind. We provide the basics, it's up to you to make it happen! This page will guide you through the steps you need to take to make growing insects for food a success!

## Preparation and unboxing
This model assumes that you have the following things in place:

  * 5V power supply with micro-usb (for example USB power source and mobile phone cable)
  * WiFi connection available, with SSID and password available
  * Grow box (IKEA Sokkerbit model)
  * 1kg mealworms (sourced from your local pet store)
  * Mealworm feed

What is in the box:

  * The v1 version of the OIF Tinker kit is a very basic setup. It is a micro-controller (the ESP8266) with a temperature and humidity sensor attached.

## Installation and configuration
This version of model Tinker uses the [ESPEasy configuration system](https://www.letscontrolit.com/wiki/index.php/Main_Page). The micro-controller comes preloaded with ESPEasy. If you somehow need to reinstall espeasy, the recommended way is to use esptool. After downloading the [latest release](https://github.com/letscontrolit/ESPEasy/releases) and unpacking it, choose the proper [binary](https://github.com/letscontrolit/ESPEasy#automated-binary-releases). Assuming you have esptool installed, run the following command (on macOS):

`esptool.py -p /dev/cu.wchusbserial1420 -b 115200 write_flash 0x00000000 ESP_Easy_mega-20190523_normal_ESP8266_4M.bin`

After powering the device, it will try to connect to a wifi network. Since no network is configured yet, it will change mode to Access Point (AP), meaning the it creates it's own wifi network. The name of the network will be something like `ESP-Easy-0`, with the default password `configesp`. The default ip-address is `192.168.4.1`. Launch your internet browser and it should find the captive portal hosted by the ESP module. You should get a welcome screen. On macOS, this does not always work, in that case browse to [192.168.4.1](http://192.168.4.1).

You can choose your WiFi network now. Select the SSID and enter your passphrase, click connect.

It will take 20 seconds until a result is shown. If you typed everything correctly it will show a message that it is connected to the network and it shows the IP address.
**Note the IP address!**
Connect your computer or whatever back to your usual network. Open a browser and type the ip address into the browser.
You should see the config pages of ESPEasy now.

The device comes with the proper configuration preloaded. If somehow the configuration is damaged, you can load the configuration file, [located here](https://gitlab.com/oif-collective). Make sure to add the proper KEY, accessible in your account settings.

When everything is connected and configured, the node will send a datapoint every 60 seconds.

**Welcome to the collective!**
