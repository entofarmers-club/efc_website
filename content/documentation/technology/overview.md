---
layout: documentation
title: Overview
subtitle: An overview of the technology stack
category: Technology
order: 10
---

{:.is-size-5}
The Technology stack is a combination of different hardware and software components. In general, the system can be divided into 3 main elements: the server, the gateway and the nodes.
The *server* is the cloud server that is the heart of the farmers collective. All the OIF farms are connected to this server. Each farm has (at least) 1 *gateway*. This device is the heart of the farm, and is connected to the server using a permanent internet connection. Various *nodes* are connected (wired or wireless) to the gateway. Each node has 1 or more sensors or actuators attached.

{:.has-text-centered}
  **Server** *1-->n* **Gateway** *1-->n* **Node**

## Server
The OIF server is a linux based distribution running various services making it possible to connect farms. The core of the server is a [NodeJS](https://nodejs.org){:target="&#95;blank"} based API which makes it possible to communicate with the server. This can be done by sending http requests, or by using a permanent [MQTT](https://mqqtt.org){:target="&#95;blank"} connection.
All time series data is stored in a [TimescaleDB time-series database](https://timescale.com){:target="&#95;blank"} (based on [postgresql](https://postgresql.org){:target="&#95;blank"}).

*Read more about the server [here]({{ site.baseurl }}/documentation/technology/server)*

## Gateway
The gateway is the heart of the farm. It is the brain that connects all nodes and has a permanent connection with the backend server. The gateway is a standalone computer, such as a [Raspberry Pi](https://raspberrypi.org){:target="&#95;blank"}. The gateway is connected to the server using WiFi (other connections will be available in the future). The nodes are connected by wire (one-wire), 2,4GHz wireless connection, or wireless over wifi. The setup can be adjusted based on each farm specification.
A farmer can log into the gateway and adjust the configuration and settings. This makes it possible to add or remove nodes, and the settings of each node.
All data is tunneled into the gateway and it is its job to process this data and send it to the backend. To achieve this, a permanent socket connection using mqtt is created.

*Read more about the gateway [here]({{ site.baseurl }}/documentation/technology/gateway)*

## Nodes
To monitor and control the farm, different sensors and actuators are connected to micro-controllers. These are called nodes. Each node can have its own configuration. The type of sensor, actuator and micro-controller is not fixed. There is a list of preferred hardware, but anyone can add their own device and software to this list.

*Read more about the nodes [here]({{ site.baseurl }}/documentation/technology/nodes)*

## Connectivity
There are multiple ways to connect the various components together:

  * **WiFi**  
    This is the most common use of the system. The gateway and nodes are connected to the same wifi network and are able to communicate over this wifi network. It does require some configuration like setting up the network settings and credentials.
  * **One wire**  
    The most reliable connection between the nodes and the gateway is by using one wire. This is a single wire that can have up tp 254 nodes attached.
  * **NRF24L01+ Radio**
    The nodes and gateway can connect over 2.4Ghz radio using the nrf24L01+ radio devices.

## Power
At this point, we assume a 220V power supply. In a later stage, we would like to make the system as standalone as possible, for instance using a PV panel and 12V power supply.
