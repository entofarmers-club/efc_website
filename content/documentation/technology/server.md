---
layout: documentation
title: Server
subtitle: The OIF collective backend
category: Technology
order: 20
---

{:.is-size-5}
This section describes the backend server of the OIF Collective system. It is possible to setup your own instance of this server. However, you are encouraged to use the collective's backend, to leverage all possibilities, and to save time setting up and maintaining the server.

This page should be the primary resource in setting up and maintaining the backend service of OIF Collective.

## Server installation
The OIF Server runs on any linux distribution on a server with internet access. This section describes the basic setup for an OIF server.

### VPS
The most most common use case is to run the server software on a VPS. This can be any provider, but we prefer a [Digital Ocean](https://digitalocean.com){:target="&#95;blank"} droplet. The preferred Linux distribution is Ubuntu 18.04 LTS.

We won't go into details about setting up a VPS instance, there are already great resources for this. We encourage you to follow these tutorials:

  * [Initial server setup](https://www.digitalocean.com/community/tutorials/initial-server-setup-with-ubuntu-18-04){:target="&#95;blank"}
  * Use ansible with this [playbook](https://www.digitalocean.com/community/tutorials/automating-initial-server-setup-with-ansible-on-ubuntu-18-04)

### Security
Security is of great importance: this is a list of interesting and necessary steps to take [[src](https://serverfault.com/questions/850283/new-ubuntu-server-best-practice-for-setup)]:

 * Create a non-root user [
 [2](https://www.digitalocean.com/community/questions/what-do-you-do-with-your-first-five-minutes-on-a-new-server){:target="&#95;blank"},
 [3](https://ryaneschinger.com/blog/securing-a-server-with-ansible/){:target="&#95;blank"},
 [7](https://www.linode.com/docs/security/securing-your-server){:target="&#95;blank"},
 [8](https://support.rackspace.com/how-to/linux-server-security-best-practices/){:target="&#95;blank"} ]
 * Add non-root to the sudoers group [
 <a href="https://www.digitalocean.com/community/questions/what-do-you-do-with-your-first-five-minutes-on-a-new-server" rel="nofollow noreferrer">2</a>,
 <a href="https://ryaneschinger.com/blog/securing-a-server-with-ansible/" rel="nofollow noreferrer">3</a>,
 <a href="https://support.rackspace.com/how-to/linux-server-security-best-practices/" rel="nofollow noreferrer">8</a> ]
 * Add public ssh key to non-root user [
 <a href="https://www.digitalocean.com/community/tutorials/7-security-measures-to-protect-your-servers" rel="nofollow noreferrer">1</a>,
 <a href="https://www.digitalocean.com/community/questions/what-do-you-do-with-your-first-five-minutes-on-a-new-server" rel="nofollow noreferrer">2</a>,
 <a href="https://ryaneschinger.com/blog/securing-a-server-with-ansible/" rel="nofollow noreferrer">3</a>,
 <a href="https://support.rackspace.com/how-to/linux-server-security-best-practices/" rel="nofollow noreferrer">8</a> ]
 * Deny all inbound traffic with ufw firewall [<a href="https://www.digitalocean.com/community/tutorials/7-security-measures-to-protect-your-servers" rel="nofollow noreferrer">1</a>, <a href="https://ryaneschinger.com/blog/securing-a-server-with-ansible/" rel="nofollow noreferrer">3</a>, <a href="https://github.com/MartynKeigher/ghost-on-digitalocean-512MB" rel="nofollow noreferrer">4</a>, <a href="https://www.linode.com/docs/security/securing-your-server" rel="nofollow noreferrer">7</a>]
 * Open required ports within the ufw firewall [<a href="https://www.digitalocean.com/community/tutorials/7-security-measures-to-protect-your-servers" rel="nofollow noreferrer">1</a>,
 <a href="https://ryaneschinger.com/blog/securing-a-server-with-ansible/" rel="nofollow noreferrer">3</a>, <a href="https://github.com/MartynKeigher/ghost-on-digitalocean-512MB" rel="nofollow noreferrer">4</a>,
 <a href="https://www.linode.com/docs/security/securing-your-server" rel="nofollow noreferrer">7</a>]
 * Update SSH config - Password-less logins [<a href="https://www.digitalocean.com/community/questions/what-do-you-do-with-your-first-five-minutes-on-a-new-server" rel="nofollow noreferrer">2</a>,
 <a href="https://ryaneschinger.com/blog/securing-a-server-with-ansible/" rel="nofollow noreferrer">3</a>,
 <a href="https://www.linode.com/docs/security/securing-your-server" rel="nofollow noreferrer">7</a>,
 <a href="https://support.rackspace.com/how-to/linux-server-security-best-practices/" rel="nofollow noreferrer">8</a>,
 <a href="https://support.asperasoft.com/hc/en-us/articles/221494788-Best-practices-for-SSH-configuration" rel="nofollow noreferrer">9</a>]
 * Update SSH config - Disable root login [
 <a href="https://www.digitalocean.com/community/questions/what-do-you-do-with-your-first-five-minutes-on-a-new-server" rel="nofollow noreferrer">2</a>,
 <a href="https://ryaneschinger.com/blog/securing-a-server-with-ansible/" rel="nofollow noreferrer">3</a>,
 <a href="http://www.techrepublic.com/article/how-to-harden-ubuntu-server-16-04-security-in-five-steps/" rel="nofollow noreferrer">5</a>,
 <a href="https://www.linode.com/docs/security/securing-your-server" rel="nofollow noreferrer">7</a>,
 <a href="https://support.rackspace.com/how-to/linux-server-security-best-practices/" rel="nofollow noreferrer">8</a>,
 <a href="https://support.asperasoft.com/hc/en-us/articles/221494788-Best-practices-for-SSH-configuration" rel="nofollow noreferrer">9</a>]
 * Update SSH config - Change ssh port [<a href="https://www.digitalocean.com/community/questions/what-do-you-do-with-your-first-five-minutes-on-a-new-server" rel="nofollow noreferrer">2</a>, <a href="https://ryaneschinger.com/blog/securing-a-server-with-ansible/" rel="nofollow noreferrer">3</a>, <a href="https://www.linode.com/docs/security/securing-your-server" rel="nofollow noreferrer">7</a>, <a href="https://support.rackspace.com/how-to/linux-server-security-best-practices/" rel="nofollow noreferrer">8</a>, <a href="https://support.asperasoft.com/hc/en-us/articles/221494788-Best-practices-for-SSH-configuration" rel="nofollow noreferrer">9</a>]
 * Unattented upgrades [<a href="https://ryaneschinger.com/blog/securing-a-server-with-ansible/" rel="nofollow noreferrer">3</a>, <a href="https://github.com/MartynKeigher/ghost-on-digitalocean-512MB" rel="nofollow noreferrer">4</a>, <a href="http://www.zartl.info/?p=422" rel="nofollow noreferrer">6</a>, <a href="https://www.linode.com/docs/security/securing-your-server" rel="nofollow noreferrer">7</a>]
 * Postfix for emails [<a href="https://www.digitalocean.com/community/questions/what-do-you-do-with-your-first-five-minutes-on-a-new-server" rel="nofollow noreferrer">2</a>, <a href="https://ryaneschinger.com/blog/securing-a-server-with-ansible/" rel="nofollow noreferrer">3</a>, <a href="http://www.zartl.info/?p=422" rel="nofollow noreferrer">6</a>]
 * Logswatch to send daily summary emails [<a href="https://ryaneschinger.com/blog/securing-a-server-with-ansible/" rel="nofollow noreferrer">3</a>]
 * Fail2ban [<a href="https://www.digitalocean.com/community/questions/what-do-you-do-with-your-first-five-minutes-on-a-new-server" rel="nofollow noreferrer">2</a>, <a href="https://ryaneschinger.com/blog/securing-a-server-with-ansible/" rel="nofollow noreferrer">3</a>, <a href="https://www.linode.com/docs/security/securing-your-server" rel="nofollow noreferrer">7</a>]
 * Set the timezone and install NTP [[1](https://www.digitalocean.com/community/tutorials/how-to-set-up-time-synchronization-on-ubuntu-18-04)]
 * Secure shared memory [<a href="http://www.techrepublic.com/article/how-to-harden-ubuntu-server-16-04-security-in-five-steps/" rel="nofollow noreferrer">5</a>]
 * Add a security login banner [[<a href="http://www.techrepublic.com/article/how-to-harden-ubuntu-server-16-04-security-in-five-steps/" rel="nofollow noreferrer">5</a>]
 * Harden the networking layer [<a href="http://www.techrepublic.com/article/how-to-harden-ubuntu-server-16-04-security-in-five-steps/" rel="nofollow noreferrer">5</a>]
 * Prevent IP spoofing [<a href="http://www.techrepublic.com/article/how-to-harden-ubuntu-server-16-04-security-in-five-steps/" rel="nofollow noreferrer">5</a>]

## Docker
The OIF software is using docker for the various elements of the system. Make sure to install docker. You can use [this tutorial](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-18-04).

## Database
We use docker to create a running instance of the database, and if needed, the database management instance. Setting this up is pretty straightforward. The easiest way is to use docker-compose with the following docker-compose.yml file:

    version: "3"

    services :
      postgres:
        image: timescaledb
        environment:
          POSTGRES_HOST: postgres
          POSTGRES_USER: ${POSTGRES_USER:-user}
          POSTGRES_PASSWORD: ${POSTGRES_PASSWORD:-password}
          POSTGRES_DB: ${POSTGRES_DB:-customdb}
          PGDATA: /data/postgres
        volumes:
          - postgres:/data/postgres

      pgadmin:
        image: dpage/pgadmin4
        environment:
          PGADMIN_DEFAULT_EMAIL: ${PGADMIN_DEFAULT_EMAIL:-user@email.com}
          PGADMIN_DEFAULT_PASSWORD: ${PGADMIN_DEFAULT_PASSWORD:-admin}
        volumes:
           - pgadmin:/root/.pgadmin
        ports:
          - "${PGADMIN_PORT:-5050}:80"

    volumes:
      postgres:
      pgadmin:

Initiate this by running the command

    docker-compose up

Now you have a running instance of timescale and you can manage the database with pgadmin (when the webserver is configured).

## Webserver
The webserver we use is [nginx](https://nginx.org). Create a new configuration file: `/etc/nginx/sites-available/oif.site.com` with the following content:

    server {
      listen 80;
      server_name oif.site.com;

      location / {
        return 301 https://$host$request_uri;
      }
    }

    server {

      listen 443 ssl http2;
      listen [::]:443 ssl http2;

      server_name oif.site.com;

      root /var/www/oif.site.com/dashboard;
      charset utf_8;

      error_log /var/log/oif.site.com/error error;

      # Include the SSL configuration from cipherli.st
      ssl_ciphers 'ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-DSS-AES128-GCM-SHA256:kEDH+AESGCM:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA:ECDHE-ECDSA-AES256-SHA:DHE-RSA-AES128-SHA256:DHE-RSA-AES128-SHA:DHE-DSS-AES128-SHA256:DHE-RSA-AES256-SHA256:DHE-DSS-AES256-SHA:DHE-RSA-AES256-SHA:AES128-GCM-SHA256:AES256-GCM-SHA384:AES128-SHA256:AES256-SHA256:AES128-SHA:AES256-SHA:AES:CAMELLIA:DES-CBC3-SHA:!aNULL:!eNULL:!EXPORT:!DES:!RC4:!MD5:!PSK:!aECDH:!EDH-DSS-DES-CBC3-SHA:!EDH-RSA-DES-CBC3-SHA:!KRB5-DES-CBC3-SHA';
      ssl_prefer_server_ciphers   on;
      ssl_dhparam                 /etc/ssl/certs/dhparam.pem;

      # gzip Settings
      gzip                        on;
      gzip_comp_level             2;
      gzip_min_length             1000;
      gzip_proxied                expired no-cache no-store private auth;
      gzip_types                  text/plain application/x-javascript text/xml text/css application/xml;


      # Buffers
      client_body_buffer_size     10K;
      client_header_buffer_size   1k;
      client_max_body_size        8m;
      large_client_header_buffers 2 1k;


      # Timeouts
      client_body_timeout         12;
      client_header_timeout       12;
      keepalive_timeout           15;
      send_timeout                10;

      # Static public files
      location ~* .(jpg|jpeg|png|gif|ico|css|js)$ {
        expires                   365d;
      }

      # NodeJS API
      location /api/ {

        proxy_pass                http://localhost:8010/;
        proxy_redirect            off;

        proxy_cache_valid         200 30m;
        proxy_cache_valid         404 1m;

        proxy_ignore_headers      X-Accel-Expires Expires Cache-Control;
        proxy_ignore_headers      Set-Cookie;
        proxy_hide_header         Set-Cookie;
        proxy_hide_header         X-powered-by;
        proxy_set_header          X-Real-IP $remote_addr;
        proxy_set_header          X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header          X-Forwarded-Proto https;
        proxy_set_header          Host $http_host;
        expires 10m;		
      }

      # Socket.io
      location /socket.io/ {
        proxy_pass                http://localhost:8010;
        proxy_http_version        1.1;
        proxy_set_header          Upgrade $http_upgrade;
        proxy_set_header          Connection "upgrade";
        proxy_set_header          Host $host;
        proxy_cache_bypass        $http_upgrade;
        proxy_set_header          X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header          Host $host;
      }

      # public frontend
      location / {
        try_files                 $uri /index.html;
      }

      location /files {
        root /var/www/oif.site.com;
        if ($request_method = 'GET') {
          add_header 'Access-Control-Allow-Origin' '*';
          add_header 'Access-Control-Allow-Methods' 'GET, POST, OPTIONS';
          add_header 'Access-Control-Allow-Headers' 'DNT,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Range';
          add_header 'Access-Control-Expose-Headers' 'Content-Length,Content-Range';
        }
      }

      location ~ /.well-known {
        allow all;
      }

      ssl_certificate /etc/letsencrypt/live/oif.site.com/fullchain.pem; # managed by Certbot
      ssl_certificate_key /etc/letsencrypt/live/oif.site.com/privkey.pem; # managed by Certbot

    }

## SSL
Use letsencrypt for creating https:

    certbot -d domain.com

## Node API
The main brain of the system is the NodeJS based API. This part is not yet dockerized, and is running using [pm2](http://pm2.org).

Use the docker-compose [script](https://gitlab.com/oif-collective/oif-collective/blob/master/software/backend/docker-compose.yml)
