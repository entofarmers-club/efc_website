---
layout: documentation
title: GIT Workflow
category: Collaboration
order: 10
---

We implement the [GitFlow](http://nvie.com/posts/a-successful-git-branching-model/) strategy. This is a branching model for Git, created by Vincent Driessen. It has attracted a lot of attention because it is very well suited to collaboration and scaling the development team.

## Key Benefits
  * **Parallel Development**  
    One of the great things about GitFlow is that it makes parallel development very easy, by isolating new development from finished work. New development (such as features and non-emergency bug fixes) is done in **feature branches**, and is only merged back into main body of code when the developer(s) is happy that the code is ready for release.
    Although interruptions are a BadThing(tm), if you are asked to switch from one task to another, all you need to do is commit your changes and then create a new feature branch for your new task. When that task is done, just checkout your original feature branch and you can continue where you left off.
  * **Collaboration**  
    Feature branches also make it easier for two or more developers to collaborate on the same feature, because each feature branch is a sandbox where the only changes are the changes necessary to get the new feature working. That makes it very easy to see and follow what each collaborator is doing.
  * **Release Staging Area**  
    As new development is completed, it gets merged back into the **develop branch**, which is a staging area for all completed features that haven’t yet been released. So when the next release is branched off of **develop**, it will automatically contain all of the new stuff that has been finished.
  * **Support For Emergency Fixes**  
    GitFlow supports **hotfix branches** - branches made from a tagged release. You can use these to make an emergency change, safe in the knowledge that the hotfix will only contain your emergency fix. There’s no risk that you’ll accidentally merge in new development at the same time.


## How it works
New development (new features, non-emergency bug fixes) are built in **feature branches**:
<img src="{{ '/assets/img/GitFlowFeatureBranches.png' | prepend: site.baseurl }}">

Feature branches are branched off of the **develop branch**, and finished features and fixes are merged back into the **develop branch** when they’re ready for release.  
<img src="{{ '/assets/img/GitFlowDevelopBranch.png' | prepend: site.baseurl }}">

When it is time to make a release, a **release branch** is created off of **develop**.  
<img src="{{ '/assets/img/GitFlowReleaseBranch.png' | prepend: site.baseurl }}">

The code in the **release branch** is deployed onto a suitable test environment, tested, and any problems are fixed directly in the release branch. **This deploy -> test -> fix -> redeploy -> retest** cycle continues until you’re happy that the release is good enough to release to customers.  
When the release is finished, the **release branch** is merged into **master** and into **develop** too, to make sure that any changes made in the **release branch** aren’t accidentally lost by new development.  
<img src="{{ '/assets/img/GitFlowMasterBranch.png' | prepend: site.baseurl }}">

The **master branch** tracks released code only. The only commits to **master** are merges from **release branches** and **hotfix branches**.
**Hotfix branches** are used to create emergency fixes.
<img src="{{ '/assets/img/GitFlowHotfixBranch.png' | prepend: site.baseurl }}">

They are branched directly from a tagged release in the **master branch**, and when finished are merged back into both master and develop to make sure that the hotfix isn’t accidentally lost when the next regular release occurs.

## Main Branches

The main repository will always hold two evergreen branches:

  * `master`
  * `develop`

The main branch should be considered `origin/develop` and will be the main branch where the source code of `HEAD` always reflects a state with the latest delivered development changes for the next release. As a developer, you will be branching and merging from `develop`.

Consider `origin/master` to always represent the latest code deployed to production. During day to day development, the `master` branch will not be interacted with.

When the source code in the `develop` branch is stable and has been deployed, all of the changes will be merged into `master` and tagged with a release number.

## Supporting Branches
Supporting branches are used to aid parallel development between team members, ease tracking of features, and to assist in quickly fixing live production problems. Unlike the main branches, these branches always have a limited life time, since they will be removed eventually.

The different types of branches we may use are:

  * Feature branches
  * Bug branches
  * Hotfix branches

Each of these branches have a specific purpose and are bound to strict rules as to which branches may be their originating branch and which branches must be their merge targets. Each branch and its usage is explained below.

### Feature Branches
Feature branches are used when developing a new feature or enhancement which has the potential of a development lifespan longer than a single deployment. When starting development, the deployment in which this feature will be released may not be known. No matter when the feature branch will be finished, it will always be merged back into the develop branch.

During the lifespan of the feature development, the lead should watch the `develop` branch to see if there have been commits since the feature was branched. Any and all changes to `develop` should be merged into the feature before merging back to `develop`; this can be done at various times during the project or at the end, but time to handle merge conflicts should be accounted for.

`<issue_number>` represents the issue-id where the feature is defined.
`<tile-of-the-issue>` represents the issue-id where the feature is defined.

  * Must branch from: `develop`
  * Must merge back into: `develop`
  * Branch naming convention: `feature/<issue_number>-<tile-of-the-issue>`

#### Working with a feature branch
If the branch does not exist yet (check with the Lead), create a new issue with a `feature` label. In the issue page on gitlab, create a new branch using the `create branch` button under the issue title.  
The branch will be named `feature/issue_id-title-of-the-feature`. A feature branch should always be 'publicly' available. That is, development should never exist in just one developer's local branch.
Fetch the new branch locally.

Periodically, changes made to `develop` (if any) should be merged back into your feature branch.

    $ git merge develop             // merges changes from master into feature branch

When development on the feature is complete, the lead (or engineer in charge) should merge changes into `develop` and then make sure the remote branch is deleted.

    $ git checkout master           // change to the master branch  
    $ git merge --no-ff feature-id  // makes sure to create a commit object during merge
    $ git push origin master        // push merge changes
    $ git push origin :feature-id   // deletes the remote branch

### Hotfix Branches

A hotfix branch comes from the need to act immediately upon an undesired state of a live production version. Additionally, because of the urgency, a hotfix is not required to be be pushed during a scheduled deployment. Due to these requirements, a hotfix branch is always branched from a tagged `master` branch. This is done for two reasons:

  * Development on the `develop` branch can continue while the hotfix is being addressed.
  * A tagged `master` branch still represents what is in production. At the point in time where a hotfix is needed, there could have been multiple commits to `develop` which would then no longer represent production.

`<issue_id>` represents the Basecamp project to which Project Management will be tracked.

  * Must branch from: tagged `master`
  * Must merge back into: `master` and `develop`
  * Branch naming convention: `hotfix/<issue_id>`

#### Working with a hotfix branch

If the branch does not exist yet (check with the Lead), create the branch locally and then push to GitHub. A hotfix branch should always be 'publicly' available. That is, development should never exist in just one developer's local branch.

    $ git checkout -b hotfix-id master  // creates a local branch for the new hotfix
    $ git push origin hotfix-id         // makes the new hotfix remotely available

When development on the hotfix is complete, [the Lead] should merge changes into `master` and then update the tag.

    $ git checkout master               // change to the stable branch
    $ git merge --no-ff hotfix-id       // forces creation of commit object during merge
    $ git tag -a <tag>                  // tags the fix
    $ git push origin master --tags     // push tag changes

Merge changes into `master` so not to lose the hotfix and then delete the remote hotfix branch.

    $ git checkout master               // change to the master branch
    $ git merge --no-ff hotfix-id       // forces creation of commit object during merge
    $ git push origin master            // push merge changes
    $ git push origin :hotfix-id        // deletes the remote branch
