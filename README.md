# About OIF Collective

This is the site and documentatin for OIF Collective

## Contributing

Bug reports and pull requests are welcome on GitLab at [OIF Collective Pages](https://gitlab.com/oif-collective/oif-collective/).

## Development

To set up your environment to develop this site, run `bundle install`.

Then run `bundle exec jekyll serve` and open your browser at `http://localhost:4000`. This starts a Jekyll server using this site.
