const path = require('path');


/**
 * Implement Gatsby's Node APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/node-apis/
 */

exports.createPages = async ({ actions, graphql, reporter }) => {

  const { createPage } = actions

  // Create main home page
  createPage({
    path: `/`,
    component: path.resolve(`./src/templates/index.js`)
  })

  // ARTICLES
  const articleResult = await graphql(`{

    allMarkdownRemark (
      sort: { order: DESC, fields: [frontmatter___date] }
      limit: 1000,
      filter: { 
        frontmatter: { 
          type: { eq: "article" }
        }
      }
    ) {
      edges {
        node {
          id
          timeToRead
          excerpt(pruneLength: 80)
          html
          frontmatter {
            type
            title
            path
            date
            featured_img {
              childImageSharp {
              fluid(maxWidth: 2800) {
                  base64
                  tracedSVG
                  aspectRatio
                  src
                  srcSet
                  srcWebp
                  srcSetWebp
                  sizes
                }
              }
            }
          } 
        }
      }
    }
  }`)
  
  // Handle errors
  if (articleResult.errors) {
    console.warn(articleresult.errors);
    reporter.panicOnBuild(`Error while running GraphQL query.`)
    return
  }

  // Create pages
  const articlePostTemplate = path.resolve(`./src/templates/article.js`)
  const articles = articleResult.data.allMarkdownRemark.edges; 
  articles.forEach((post, index) => {
    
    const previous = index === articles.length - 1 ? null : articles[index + 1].node
    const next = index === 0 ? null : articles[index - 1].node
    createPage({
      path: post.node.frontmatter.path, 
      component: articlePostTemplate,
      context: {
        id: post.node.id,
        previous, 
        next,
      }
    })
  })

  const postsPerPage = 12
  const numPages = Math.ceil(articles.length / postsPerPage)
  const articlePageTemplate = path.resolve('./src/templates/article-list-template.js')
  
  Array.from({ length: numPages }).forEach((_, i) => {
  
    createPage({
      path: i === 0 ? `/articles` : `/articles/${i + 1}`,
      component: articlePageTemplate,
      context: {
        limit: postsPerPage,
        skip: i * postsPerPage,
        numPages,
        currentPage: i + 1,
      }
    })
  })
}
